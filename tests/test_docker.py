import pytest
import requests
import base64
import json
from tensorflow.keras.datasets.mnist import load_data
import numpy as np
 #Importing general packages
import numpy as np
import matplotlib.pyplot as plt

#Model packages
import tensorflow as tf
from tensorflow.keras import layers

#File saving packages
import warnings
warnings.filterwarnings('ignore')

#load Plant dataset
#Reading train csv (index col = 0 as need to copy images into train folder)

train_ds = tf.keras.utils.image_dataset_from_directory(
    '/root/ca2-2b01-huilynliuzhen-tf/model/imagesForTesting/test',
    seed=123,
    image_size=(180, 180),
    batch_size=32,
    label_mode='categorical'
)
print("TRAIN DS IS", train_ds)
train_ds2 = list(train_ds)
X_train = list(map(lambda x: x[0], train_ds2))
y_train = list(map(lambda x: x[1], train_ds2)) 

X_train = X_train[0].numpy()
y_train = y_train[0].numpy()

# # reshape data to have a single channel
# x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], x_test.shape[2], 1))
# # normalize pixel values
# x_test = x_test.astype('float32') / 255.0
 
#server URL
url = 'https://plantserver420.herokuapp.com/v1/models/img_classifier:predict'
 
def make_prediction(instances):
  data = json.dumps({"signature_name": "serving_default",
                     "instances": instances.tolist()})
  headers = {"content-type": "application/json"}
  json_response = requests.post(url, data=data, headers=headers)
  predictions = json.loads(json_response.text)['predictions']
  return predictions
    
def test_prediction():
  predictions = make_prediction(X_train[0:4])
  for i, pred in enumerate(predictions):
    print("pred is ",pred)
    print("y train is ", y_train[i])
    predMaxValue = max(pred)
    predMaxIndex = pred.index(predMaxValue)
    yTrainMaxIndex = np.argmax(y_train[i])
    assert yTrainMaxIndex == predMaxIndex